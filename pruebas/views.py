from django.http import HttpResponse
from .models import *
from django.core import serializers
from config import *
from moodle import *
import json

def index(request):
    return HttpResponse("Indice")

def students(request):
    get_students = Student.objects.all()
    output = '<br/>'.join([str(n.id)+','+n.nickname+','+str(n.level) for n in get_students])
    data = serializers.serialize("json", Student.objects.all())
    return HttpResponse(data)

def subjects(request):
    get_subjects = Subject.objects.all()

    output = '<br/>'.join([n.name for n in get_subjects])
    data = serializers.serialize("json", Subject.objects.all())
    return HttpResponse(data)

def students_from_subject(request, subject_id):
    response = Subject.objects.get(id=subject_id).student_set.all()
    output = '<br/>'.join([n.nickname for n in response])
    data = serializers.serialize("json", response)
    return HttpResponse(data)

def subjects_from_student(request, student_id):
    response = Student.objects.get(id=student_id).subjects.all()
    output = '<br/>'.join([n.name for n in response])
    data = serializers.serialize("json", response)
    return HttpResponse(data)

def tasks(request):
    get_tasks = Task.objects.all()
    output = '<br/>'.join([n.name for n in get_tasks])
    data = serializers.serialize("json", get_tasks)
    return HttpResponse(data)

def tasks_from_subject(request, subject_id):
    response = Subject.objects.get(id=subject_id).tasks.all()
    output = '<br/>'.join([n.name for n in response])
    data = serializers.serialize("json", response)
    return HttpResponse(data)

def retrieve_subjects(request):
    mdl = MDL()
    # xmlrpc Connection

    """
    Get courses
    """

    return HttpResponse(mdl.get_courses(admin_server))

    jsons = mdl.get_courses(admin_server)
    decjsons = jsons.decode(encoding='utf-8')

    try:
        decoded = json.loads(decjsons)
        # Access data
        for x in decoded:
            nsubject = Subject(mid=x['id'], name=x['shortname'])
            nsubject.save()

    except (ValueError, KeyError, TypeError):
        print "JSON format error"

    return HttpResponse(Subject.objects.all())

def retrieve_student_subjects(request, mstudent_id):

    mdl = MDL()
    params = {
       'userid': mstudent_id,
    }
    return HttpResponse(mdl.core_enrol_get_users_courses(admin_server, params))

def retrieve_assignments(request,student_id):
    mdl = MDL()
    params = {}
    return HttpResponse(mdl.assign_get_assignments(get_server(Student.objects.get(pk=student_id).token), params))

def get_user_token(request,user,passw):
    mdl = MDL()
    return HttpResponse(mdl.get_token(nt_server,user,passw))

def retrieve_course_users(request,subject_id):
    mdl = MDL()
    params = {
        'courseid': subject_id,
    }
    return HttpResponse(mdl.get_course_users(admin_server, params))

def get_course_detail(request,subject_id):
    mdl = MDL()
    params = {
        'field': 'id',
        'value': subject_id,
    }
    return HttpResponse(mdl.get_couse_detail(admin_server,params))