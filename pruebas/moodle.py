class MDL:
    """
    Main class to connect Moodle webservice
    More information about Webservice:
        http://docs.moodle.org/dev/Web_Services_API
        http://docs.moodle.org/dev/Web_services
        http://docs.moodle.org/dev/Creating_a_web_service_client
        http://docs.moodle.org/dev/Web_services_Roadmap#Web_service_functions
    """

    """
    Moodle Connection Methods available: XML-RPC, REST
    TODO: SOAP, AMF Protocols
    """

    def get_token(self,server,user,passw):

        import urllib2
        import json
        from .models import *
        from DBManager import create_standard_student

        if 'uri' not in server:
            return False

        if server['uri'][-1] == '/':
            server['uri'] = server['uri'][:-1]

        url = '%s/login/token.php?username=%s&password=%s&service=GameAPI'%(server['uri'],user,passw)

        request = urllib2.Request(url)
        result = urllib2.urlopen(request)

        r = result.read()

        decjsons = r.decode(encoding='utf-8')

        try: #Comprobar que el JSON es correcto
            decoded = json.loads(decjsons)
        except (ValueError, KeyError, TypeError):
            print "JSON format error"

        try: #Comprobar que el usuario esta en moodle
            token = decoded['token']
        except (KeyError):
            print (decoded['error'])
            return json.loads('{"result":"false"}')['result']

        # Comprobar si el usuario esta en nuestra BDD

        if(Student.objects.filter(muser=user).exists()):
            student = Student.objects.get(muser=user)
            student.token = token
            student.save()
            print('Student_pk de %s es %s'%(user,student.pk))
            return json.loads('{"result":"true"}')['result']
        else:
            student = create_standard_student()
            student.muser = user
            student.token = token
            student.save()
            return json.loads('{"result":"new"}')['result']

        #Recuperar las asignaturas que cursa el usuario

    def add_course(self,server,m_id):

        return

    def get_couse_detail(self,server,params):
        if 'protocol' not in server:
            return False

        function = 'core_course_get_courses_by_field'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)

    def core_enrol_get_users_courses(self, server, params):
        if 'protocol' not in server:
            return False

        function = 'core_enrol_get_users_courses'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)

    def conn_rest(self, server, function):
        """
        Connection to Moodle with REST Webservice
        server = {
            'protocol': 'rest',
            'uri': 'http://www.mymoodle.org',
            'token': 'mytokenkey',
        }
        """
        import urllib2
        import json

        if 'uri' not in server or 'token' not in server:
            return False

        if server['uri'][-1] == '/':
            server['uri'] = server['uri'][:-1]

        url = '%s/webservice/%s/server.php' % (server['uri'], 'rest')
        data = 'wstoken=%s&wsfunction=%s' % (server['token'], function) + '&moodlewsrestformat=json'

        request = urllib2.Request(url, data)
        result = urllib2.urlopen(request)
        return result

    def rest_protocol(self, server, params, function=None, key_word=None):
        """
        Construct the correct function to call
        """
        if function is None:
            function = ""
        if key_word is None:
            key_word = ""
        count = 0
        if params != '':
            for key, value in params.items():
                if type(value) is dict:
                    for item in iter(value):
                        function += '&%s[%s][%s]=' % (key, str(count), item)
                        function += '%s' % value[item]
                else:
                    function += '&%s=' % (key)
                    function += '%s' % value
                count += 1

        return self.conn_rest(server, function)

    def conn_xmlrpc(self, server, service=None, params=None):
        """
        Connection to Moodle with XML-RPC Webservice
        server = {
            'protocol': 'xmlrpc',
            'uri': 'http://www.mymoodle.org',
            'token': 'mytokenkey',
        }
        """
        if 'uri' not in server or 'token' not in server:
            return False

        import xmlrpclib

        if server['uri'][-1] == '/':
            server['uri'] = server['uri'][:-1]

        url = '%s/webservice/%s/server.php?wstoken=%s' % (server['uri'], server['protocol'], server['token'])
        return xmlrpclib.ServerProxy(url)

    def xmlrpc_protocol(self, server, params, function=None, key_word=None):
        """
        Select the correct function to call
        """

        def core_course_get_courses(params):
            return proxy.core_course_get_courses()

        def core_course_get_courses_by_field(params):
            return proxy.core_course_get_courses_by_field()

        def core_course_update_courses(params):
            return proxy.core_course_update_courses(params)

        def core_enrol_get_users_courses(params):
            return proxy.core_enrol_get_users_courses(params)

        def core_enrol_get_enrolled_users(params):
            return proxy.core_enrol_get_enrolled_users(params)

        def core_user_get_users(params):
            return proxy.core_user_get_users(params)

        def core_user_update_users(params):
            return proxy.core_user_update_users(params)

        def mod_assign_get_assignments(params):
            return proxy.mod_assign_get_assignments(params)


        def not_implemented_yet(params):
            return False

        proxy = self.conn_xmlrpc(server)
        select_method = {
            "core_course_get_courses": core_course_get_courses,
            "core_course_get_courses_by_field": core_course_get_courses_by_field,
            "core_course_update_courses": core_course_update_courses,
            "core_enrol_get_users_courses": core_enrol_get_users_courses,
            "core_enrol_get_enrolled_users": core_enrol_get_enrolled_users,
            "mod_assign_get_assignments": mod_assign_get_assignments,

            "not_implemented_yet": not_implemented_yet,
        }

        if function is None or function not in select_method:
            function = "not_implemented_yet"

        return select_method[function](params)

    def get_courses(self, server):
        """
        Get all courses
        Input:
            server = {
                'protocol': 'xmlrpc|rest',
                'uri': 'http://www.mymoodle.org',
                'token': 'mytokenkey',
            }
        Output:
            xmlrpc protocol:    list of dictionaries
            rest protocol:      xml file format
        """
        if 'protocol' not in server:
            return False
        params = ''
        function = 'core_course_get_courses'
        # key_word = ''
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)

    def assign_get_assignments (self, server, params):

        if 'protocol' not in server:
            return False
        function = 'mod_assign_get_assignments'

        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)

    def get_users(self, server, params):
        """
        Get users
        Input:
            server = {
                'protocol': 'xmlrpc|rest',
                'uri': 'http://www.mymoodle.org',
                'token': 'mytokenkey',
            }
            params = [{                    # Input a list of dictionaries
                'key' : key_name (fullname,username,email....)
                'value': value to search
            }]
        Output:
            xmlrpc protocol:    list of dictionaries
            rest protocol:      xml file format
        params example:   # criteria = [{'key':'username','value':'api_user'}]
                          # mdl.get_users(server,criteria)
        """
        if 'protocol' not in server:
            return False
        function = 'core_user_get_users'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)

    def update_users(self, server, params):
        """
        Update the users information
        Input:
            server = {
                'protocol': 'xmlrpc|rest',
                'uri': 'http://www.mymoodle.org',
                'token': 'mytokenkey',
            }
            params = [{                     # Input a list of dictionaries
                'id': 2,                    # Required & unique
                'firstname': firstname,     # Value to modify
            }]
        Output:
            xmlrpc protocol:    None
            rest protocol:      None
        """
        if 'protocol' not in server:
            return False
        function = 'core_user_update_users'
        key_word = 'users'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function, key_word)

    def get_course_users(self, server, params):
        """
            protocol XML-RPC is broken for this method
        """
        if 'protocol' not in server:
            return False
        function = 'core_enrol_get_enrolled_users'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }

        return protocol['rest'](server, params, function)
        #return protocol[server['protocol']](server, params, function)

    def update_course(self, server, params):
        """
        """
        if 'protocol' not in server:
            return False
        function = 'core_course_update_courses'
        protocol = {
            "xmlrpc": self.xmlrpc_protocol,
            "rest": self.rest_protocol,
        }
        return protocol[server['protocol']](server, params, function)