from __future__ import unicode_literals

from django.db import models
class Task(models.Model):
    mid = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    gradeable = models.BooleanField(default=False)
    type = models.CharField(max_length=20)
    relevance = models.IntegerField()
    try:
        def __str__(self):
            return self.name.encode('UTF8')
    except UnicodeEncodeError:
        def __unicode__(self):
            return u'{c}'.format(c=self.char.name.encode('UTF8'))

class Subject(models.Model):
    mid = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    tasks = models.ManyToManyField(Task)
    try:
        def __str__(self):
            return self.name.encode('UTF8')
    except UnicodeEncodeError:
        def __unicode__(self):
            return u'{c}'.format(c=self.char.name.encode('UTF8'))


class Familiar(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    level = models.IntegerField()
    attack = models.IntegerField()
    deffense = models.IntegerField()
    s_attack = models.IntegerField()
    s_deffense = models.IntegerField()
    max_hp = models.IntegerField()
    def __str__(self):
        return self.name

class Instrument(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    def __str__(self):
        return self.name
class Student(models.Model):
    mid = models.IntegerField(default=0)
    nickname = models.CharField(max_length=50)
    muser = models.CharField(max_length=50, default='')
    token = models.CharField(max_length=50, default='')
    level = models.IntegerField()
    attack = models.IntegerField()
    deffense = models.IntegerField()
    s_attack = models.IntegerField()
    s_deffense = models.IntegerField()
    max_hp = models.IntegerField()
    max_energy = models.IntegerField()
    current_energy = models.IntegerField()
    last_quest = models.IntegerField()
    energy_obj = models.IntegerField()
    subjects = models.ManyToManyField(Subject, blank=True, through='student_subject')
    completed_tasks = models.ManyToManyField(Task, blank=True)
    familiars = models.ManyToManyField(Familiar, blank=True)
    instruments = models.ManyToManyField(Instrument, blank=True)
    def __str__(self):
        return str(self.id) +', ' + self.nickname

class student_subject(models.Model):
    student_id = models.ForeignKey(Student)
    subject_id = models.ForeignKey(Subject)
    profesor = models.BooleanField()


