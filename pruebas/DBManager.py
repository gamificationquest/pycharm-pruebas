from django.http import HttpResponse
from .models import *
from django.core import serializers
from config import *
from moodle import *
import json

def create_standard_student():

    st = Student(
        mid=0,
        nickname='',
        muser='',
        token='',
        level=1,
        attack=10,
        deffense=10,
        s_attack=10,
        s_deffense=10,
        max_hp=30,
        max_energy=50,
        current_energy=50,
        last_quest=0,
        energy_obj=0,
    )

    return st

def create_standard_subject():
    sj = Subject(
        mid=0,
        name='',
    )

    return sj