from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    # ex: index/students/
    url(r'^students/$', views.students, name='students'),
    # ex: index/subjects/
    url(r'^subjects/$', views.subjects, name='subjects'),
    # ex: /students/1/subjects/
    url(r'^students/(?P<student_id>[0-9]+)/subjects/$', views.subjects_from_student, name='subjects_from_student'),
    # ex: /subjects/1/students/
    url(r'^subjects/(?P<subject_id>[0-9]+)/students/$', views.students_from_subject, name='students_from_subject'),
    # ex: /tasks/
    url(r'^tasks/$', views.tasks, name='tasks'),
    # ex: /subjects/1/tasks/
    url(r'^subjects/(?P<subject_id>[0-9]+)/tasks/$', views.tasks_from_subject, name='tasks_from_subject'),
    # ex: /msubjects/
    url(r'^msubjects/$', views.retrieve_subjects, name='retrieve_subjects'),

    url(r'^msubjects/(?P<subject_id>[0-9]+)/users/$', views.retrieve_course_users, name='tasks_from_subject'),
    # ex: /mstudents/
    url(r'^mstudents/(?P<mstudent_id>[0-9]+)/msubjects/$', views.retrieve_student_subjects, name='retrieve_student_subjects'),
    # ex:
    url(r'^mstudent/(?P<student_id>[0-9]+)/massignments$', views.retrieve_assignments, name='retrieve_assignments'),

    url(r'^token/(?P<user>.+)/(?P<passw>.+)/$', views.get_user_token, name='get_user_token'),

    # Recoger informacion de un curso en concreto
    url(r'^course/(?P<subject_id>[0-9]+)/$', views.get_course_detail, name='get_course_detail'),

    #Recoger las tareas de un curso en concreto
    #url(r'^course/(?P<subject_id>[0-9]+)/activities/$', views.get_course_activities, name='get_course_detail'),
]
