from django.contrib import admin

from .models import Student
from .models import Subject
from .models import Task
from .models import Instrument
from .models import Familiar

admin.site.register(Student)
admin.site.register(Subject)
admin.site.register(Task)
admin.site.register(Instrument)
admin.site.register(Familiar)